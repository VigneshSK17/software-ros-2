#!/usr/bin/env bash

source scripts/utils.sh --prefix "$0"
shopt -s globstar

fix=

while [ "$1" != "" ]; do
  case $1 in
  --fix | -f)
    fix=1
    ;;
  esac
  shift
done

if [ "$fix" == "1" ]; then
  prefix_run cmake-format --in-place src/*/**/CMakeLists.txt
else
  prefix_run cmake-format --check src/*/**/CMakeLists.txt
fi