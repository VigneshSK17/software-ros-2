# Steps to Launch a Dynamic Obstacle Simulation:

1. Build the plugin

```
cd sprite/src/sprite/launch/dynamic_obstacle/animated_box/build
make
export GAZEBO_PLUGIN_PATH=`pwd`:$GAZEBO_PLUGIN_PATH
```

2. Run the launch file.

- To run the animated box world with Gazebo and rviz, run

```
roslaunch sprite dynamic_gui.launch

```

- To run the cafe world with Gazebo and rviz, run

```
roslaunch sprite cafe_gui.launch
```

**NOTE:**

- Parameters to edit the box's movement are found in sprite/src/sprite/launch/dynamic_obstacle/animated_box/animated_box.cc
- If you edit animated_box.cc, independent_listener.cc, or integrated_mai.cc, you must run:

```
cd sprite/src/sprite/launch/dynamic_obstacle/animated_box/build
make
```

in order to see the changes you've made.

**NOTE:**
If things are not working after these steps try turning your computer off and on. It actually works.

# Steps to launch Actor Collisions

1. Build the plugin

```
cd sprite/src/sprite/launch/dynamic_obstacle/actor_collisions/build
make
export GAZEBO_PLUGIN_PATH=`pwd`:$GAZEBO_PLUGIN_PATH
```

This allows the actor to kick around the objects when it walks around.

2. Run the launch file.

```
- To run the cafe world with Gazebo and rviz, run
```

roslaunch sprite cafe_gui.launch

# Actor Collisions Plugin

This plugin example demonstates how to enable collisions for an
animated actor and scale them.

Key points:

- The plugin must call `Link::Init` on each of the actor's links so collisions
  are enabled on the physics engine

- When using ODE as the physics engine, it is necessary to set
  `<allow_auto_disable>` to false on models which should collide with the actor.
  This does not mean that setting it to true will prevent collisions.

- The collision names used on the SDF file take the following format:

  <node*name>*<node_name>\_collision

  Where the nodes are described on the skeleton's COLLADA file.

## Generate new worlds

The example world was generated from a template that randomizes various shapes
parameters. You can generate new ones from this directory as follows:

    erb actor_collisions.world.erb > actor_collisions.world
