function [laser_scan] = convert_augmented_scan(lidar_scan)

laser_scan.angle_min = lidar_scan.angle(1);
laser_scan.angle_max = lidar_scan.angle(end);
laser_scan.angle_increment = (laser_scan.angle_max - laser_scan.angle_min) / height(lidar_scan);

laser_scan.scan_time = (lidar_scan.time(end) - lidar_scan.time(1)) / 1000000;
laser_scan.time_increment = laser_scan.scan_time / height(lidar_scan);

laser_scan.range_min = 0;
laser_scan.range_max = 175;
laser_scan.ranges = interp1(lidar_scan.angle, lidar_scan.dist, laser_scan.angle_min:laser_scan.angle_increment:laser_scan.angle_max);

laser_scan.raw_data = lidar_scan;

end

