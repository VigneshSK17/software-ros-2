function [lidar_scans] = split_lidar_data(lidar_data)

num_readings = height(lidar_data);

i = 1; j = 1;
while j < num_readings
    prev_angle = -1;
    while j < num_readings && lidar_data.angle(j) >= prev_angle
        prev_angle = lidar_data.angle(j); j = j + 1;
    end
    scan_start = j;
    
    prev_angle = -1;
    while j < num_readings && lidar_data.angle(j + 1) >= prev_angle
        prev_angle = lidar_data.angle(j + 1); j = j + 1;
    end
    
    lidar_scans(i,1) = {lidar_data(scan_start:j,:)}; i = i + 1; %#ok<AGROW>
end

end

