cmake_minimum_required(VERSION 2.8.3)
project(sprite_base_controller_firmware)

include_directories(${ROS_LIB_DIR})

file(GLOB_RECURSE ros_src "${ROS_LIB_DIR}/*.cpp" "${ROS_LIB_DIR}/*.h")
add_library(ros_lib ${ros_src})

import_arduino_library(Servo)
import_arduino_library(SPI)
import_arduino_library(Wire)
import_arduino_library(Adafruit_Sensor)
import_arduino_library(Adafruit_BusIO)
import_arduino_library(Adafruit_BNO055)

add_teensy_executable(main main.cpp)
target_link_libraries(main ros_lib)

find_path(
  ROOT_PROJECT_ROOT scripts/firmware/teensy-cli.sh
  HINTS . .. ../.. ../../.. ../../../..
  NO_DEFAULT_PATH)

add_custom_target(
  main-upload
  COMMAND scripts/firmware/teensy-cli.sh
          ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/main.hex
  WORKING_DIRECTORY ${ROOT_PROJECT_ROOT})
add_dependencies(main-upload main_Firmware)
