/*
 * Physical vehicle firmware
 *
 */

#include <Arduino.h>
#include <Servo.h>
#include <ros.h>
#include <ackermann_msgs/AckermannDrive.h>
#include <Adafruit_BNO055.h>
#include <std_msgs/Float64.h>
#include <sensor_msgs/Imu.h>
#include <Wire.h>

// pin setup
#define PIN_STEERING 16
#define PIN_THROTTLE 17
#define PIN_RPM 2
#define PIN_RPM_INTERRUPT 2  // TODO: Figure out why digitalPinToInterrupt(3) doesn't work.


// Steering servo limits (in degrees):
#define STEERING_FULL_LEFT 50
#define STEERING_STRAIGHT 90
#define STEERING_FULL_RIGHT 130
#define STEERING_TRIM -2

// ESC input bands, and corresponding physical speeds:
// rev max = 1320 μs => -0.95 m/s
// rev min = 1375 μs => -0.15 m/s
// fwd min = 1590 μs => 0.2 m/s
// fwd max = 1630 μs => 1.4 m/s

#define MIN_SPEED_REV          -0.15    // in m/s
#define MIN_SPEED_FWD           0.2     // in m/s

// ESC input bands, and corresponding physical speeds:
#define THROTTLE_FULL_REVERSE   1320    // in PWM µs => -0.95 m/s
#define THROTTLE_MIN_REVERSE    1375    // in PWM µs => -0.15 m/s
#define THROTTLE_STOP           1500    // in PWM µs => 0 m/s
#define THROTTLE_MIN_FORWARD    1590    // in PWM µs => 0.2 m/s
#define THROTTLE_FULL_FORWARD   1900    // in PWM µs => 1.4 m/s

// PID related constants
#define PID_FREQUENCY           10      // in Hz
#define PID_DELAY               100     // 1000/PID_FREQUENCY in milliseconds
#define PID_WAITING             10000
#define WHEEL_DIAMETER          0.105   // in meters
#define GEAR_RATIO              2.84210526316 // 54/19 unitless (rotations of gear shaft / rotation of wheel)
#define SPEED_TO_RPM            516.954401622 // 60*GEAR_RATIO/PI/WHEEL_DIAMETER
                                        // in [rpm / (m/s)] //use: speed * SPEED_TO_RPM
#define PID_kP                  0.0438  // unitless
#define PID_kI                  0.0     // unitless
#define PID_kD                  0.1     // unitless
#define PID_INTEGRAL_WINDOW     20      // rpm MUST EXCEED MINIMUM SPEEDS IN EITHER DIRECTION (-0.15, 0.2)*SPEED_TO_RPM

#define MAX_RPM_PULSE_DELAY     1161
#define MAX_CYCLES_WITHOUT_RPM_DETECTION 3

#define IMU_SAMPLE_PERIOD_MS 10

ros::NodeHandle nh;



/**
 * Steering / throttle
 **/

Servo steering;
Servo throttle;

/**
 * IMU
 */

Adafruit_BNO055 bno;
volatile bool is_IMU_updated;
volatile uint16_t imu_detection_counter;
unsigned long imu_lastUpdate;

sensor_msgs::Imu imu_msg;
// std_msgs::Float64 emptyArray[9] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
// imu_msg.orientation_covariance = emptyArray;
// imu_msg.angular_velocity_covariance = emptyArray;
// imu_msg.linear_acceleration_covariance = emptyArray;

imu::Quaternion imu_quat;
imu::Vector<3> imu_linear;
imu::Vector<3> imu_angular;

ros::Publisher pub_imu("IMU", &imu_msg);

/*
std_msgs::Float64 imu_orientation_X_msg;
ros::Publisher pub_imu_orientation_X("imu_X", &imu_orientation_X_msg);
*/

/**
 * RPM
 **/

volatile unsigned long prev_time, curr_time;
float time_diff;
volatile bool is_RPM_updated;


float currentThrottleLevel;
float newThrottleLevel;

float pid_goalRPM;                     // rpm
float pid_currentRPM;                  // rpm

float pid_prevError;                   // rpm
float pid_currError;                   // rpm
float pid_integral;                    // rpm
float pid_derivative;                  // rpm/loop
float pid_decision;

unsigned long prevPIDCalcTime;
unsigned long bootTime;
volatile uint16_t rpm_detection_counter;

inline char sign(float val) {
  return val < 0 ? -1 : (val > 0 ? 1 : 0);
}

void updateMotors() {
  if ((millis() - bootTime) < PID_WAITING || (millis() - prevPIDCalcTime) < PID_DELAY) {
    return;
  }
  pid_currError = pid_goalRPM - pid_currentRPM;
  if (abs(pid_currError) < PID_INTEGRAL_WINDOW && sign(pid_currError) == sign(pid_prevError)) {
    pid_integral += pid_currError;
  } else {
    pid_integral = 0;
  }
  pid_derivative = pid_currError - pid_prevError;

  pid_decision = pid_currError*PID_kP + pid_integral*PID_kI + pid_derivative*PID_kD;

  newThrottleLevel = (pid_decision + currentThrottleLevel);

  newThrottleLevel = min(max(newThrottleLevel, THROTTLE_FULL_REVERSE), THROTTLE_FULL_FORWARD);

  // Checks for RPM detection
  if (rpm_detection_counter <= MAX_CYCLES_WITHOUT_RPM_DETECTION) {
      // RPM detected
      digitalWrite(LED_BUILTIN, LOW);
      throttle.writeMicroseconds(newThrottleLevel);
  }
  else {
      // RPM not detected
      throttle.writeMicroseconds(THROTTLE_STOP);
      pid_goalRPM = 0;
      digitalWrite(LED_BUILTIN, HIGH);
      newThrottleLevel = THROTTLE_STOP;
  }

  currentThrottleLevel = newThrottleLevel;

  pid_prevError = pid_currError;
  prevPIDCalcTime = millis();

  if (currentThrottleLevel < THROTTLE_MIN_REVERSE || currentThrottleLevel > THROTTLE_MIN_FORWARD)
      ++rpm_detection_counter;
}

// Steering servo limits:
// full left = 50°
// center = 90°
// full right = 130°

// Converts steering angle from ROS input to Servo output
//   Input is in radians, where forward = 0 rad.
//   Output is in degrees, where forward = 90 deg.
uint16_t get_steering_angle(float angle) {
  // Convert input from radians to degrees. Apply center offset, and any trim necessary.
  return angle * 180 / PI + 90 + STEERING_TRIM;
}

// Callback function for ackermann_cmd topic
void ackermann_cb(const ackermann_msgs::AckermannDrive& ack_msg) {
  steering.write(get_steering_angle(ack_msg.steering_angle));
  pid_goalRPM = ack_msg.speed * SPEED_TO_RPM;
}

ros::Subscriber<ackermann_msgs::AckermannDrive> ackermann_sub("/ackermann_cmd" , &ackermann_cb);

// Updates RPM value if change is detected
void updateRPM() {
    if (is_RPM_updated) {
      // Extrapolate time difference over 1 minute (60,000 ms) to get RPM
      pid_currentRPM = 60000.0f / (curr_time - prev_time); //TODO: test the reverse logic

      is_RPM_updated = false;
    } else if (pid_currentRPM && millis() - curr_time > MAX_RPM_PULSE_DELAY) {
      // If RPM sensor hasn't triggered within a time period, that means we've stopped moving.
      pid_currentRPM = 0.0f;
    }
}

// RPM interrupt service routine
void rpm_isr() {
  prev_time = curr_time;
  curr_time = millis();
  is_RPM_updated = true;
  rpm_detection_counter = 0; //reset counter if detected
}

// Updates IMU values if change is detected
void updateIMU() {
  if (imu_lastUpdate - millis() > IMU_SAMPLE_PERIOD_MS) {
    imu_quat = bno.getQuat();
    imu_msg.orientation.x = imu_quat.x();
    imu_msg.orientation.y = imu_quat.y();
    imu_msg.orientation.z = imu_quat.z();
    imu_msg.orientation.w = imu_quat.w();

    //imu_orientation_X_msg.data = imu_quat.x();
    //pub_imu_orientation_X.publish(&imu_orientation_X_msg);

    imu_linear = bno.getVector(bno.VECTOR_LINEARACCEL); //VECTOR_LINEARACCEL
    imu_msg.linear_acceleration.x = imu_linear.x();
    imu_msg.linear_acceleration.y = imu_linear.y();
    imu_msg.linear_acceleration.z=  imu_linear.z();

    imu_angular = bno.getVector(bno.VECTOR_GYROSCOPE); //VECTOR_GYROSCOPE
    imu_msg.angular_velocity.x = imu_angular.x();
    imu_msg.angular_velocity.y = imu_angular.y();
    imu_msg.angular_velocity.z = imu_angular.z();

    imu_msg.header.stamp = nh.now();

    pub_imu.publish(&imu_msg);
    imu_lastUpdate = millis();
  }
}

void setupIMU() {
  Wire.setSDA(34);
  Wire.setSCL(33);
  bno = Adafruit_BNO055();
  bno.begin();
  for (int i = 0; i < 9; ++i) {
    imu_msg.orientation_covariance[i] = 0;
    imu_msg.angular_velocity_covariance[i] = 0;
    imu_msg.linear_acceleration_covariance[i] = 0;
  }
}

void setup() {
  steering.attach(PIN_STEERING);
  throttle.attach(PIN_THROTTLE);
  pinMode(PIN_RPM, INPUT_PULLUP); //sets RPM pin as Input
  attachInterrupt(PIN_RPM_INTERRUPT, rpm_isr, RISING);
  pinMode(LED_BUILTIN, OUTPUT); // Setup Built-in LED for debugging

  pid_goalRPM = 0;
  pid_prevError = 0;
  pid_integral = 0;
  pid_derivative = 0;
  prevPIDCalcTime = 0;

  currentThrottleLevel = THROTTLE_STOP;
  throttle.writeMicroseconds(currentThrottleLevel);

  bootTime = millis();
  prev_time = millis();


  setupIMU();
  imu_lastUpdate = 0;

  nh.getHardware()->setBaud(115200);    // Default baud rate is 57600
  nh.initNode();
  //nh.subscribe(ackermann_sub);
  //nh.advertise(pub_imu_orientation_X);
  nh.advertise(pub_imu);

}

void loop() {
  nh.spinOnce();
  //updateRPM();
  //updateMotors();
  updateIMU();
  delay(10);
}
