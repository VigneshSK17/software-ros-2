#!/usr/bin/env bash

usage() {
  echo "usage: . bootstrap.sh [--full]"
}

# https://stackoverflow.com/a/28776166/9313910
sourced=
if [ -n "${ZSH_EVAL_CONTEXT}" ]; then
  case ${ZSH_EVAL_CONTEXT} in *:file) sourced=1;; esac
elif [ -n "${BASH_VERSION}" ]; then
  (return 0 2>/dev/null) && sourced=1
else
  case ${0##*/} in sh|dash) sourced=1;; esac
fi

if [ -z "${sourced}" ]; then
  echo "You must source bootstrap.sh"
  usage
  exit 1 # not return because "you can only return from a function or sourced script"
fi

if [ -z "${ROS_ROOT}" ]; then
  . "/opt/ros/${ROS_DISTRO:-noetic}/setup.sh"
fi

. scripts/utils.sh --prefix bootstrap.sh


case $(uname -m) in
  x86_64)
    LINUX_ARCH=linux64
    ;;
  aarch64)
    LINUX_ARCH=linuxaarch64
    ;;
  *)
    LINUX_ARCH=unsupported
    ;;
esac

prefix_print "Detected architecture: $(uname -m) -> ${LINUX_ARCH}"
if [ ${LINUX_ARCH} == "unsupported" ]; then
  return 1
fi


no_build=
full=

while [ -n "$1" ]; do
  case $1 in
  --no-build)
    no_build=1
    ;;
  -F | --full)
    full=1
    ;;
  -h | --help)
    usage
    return 0
    ;;
  *)
    usage
    return 1
    ;;
  esac
  shift
done


# apt-install any necessary apt dependencies for bootstrap
apt_install curl



#------------------------------
# Arduino-related dependencies
#

if [ ! -f /etc/udev/rules.d/00-teensy.rules ] || [ -n "${full}" ]; then
  prefix_print "Downloading Teensy udev rules..."
  rm -f 00-teensy.rules
  curl -O https://www.pjrc.com/teensy/00-teensy.rules
  prefix_print "Installing Teensy udev rules..."
  sudo mkdir -p /etc/udev/rules.d/
  sudo mv 00-teensy.rules /etc/udev/rules.d/
fi

apt_install gcc-arm-none-eabi

if [ ! -d arduino-ide ] || [ -n "${full}" ]; then
  apt_install xz-utils
  ARDUINO_VERSION=1.8.19

  prefix_print "Downloading Arduino IDE (version ${ARDUINO_VERSION})..."
  rm -f arduino-${ARDUINO_VERSION}-${LINUX_ARCH}.tar.xz
  curl -O "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${LINUX_ARCH}.tar.xz"

  prefix_print "Extracting Arduino IDE..."
  tar xf "arduino-${ARDUINO_VERSION}-${LINUX_ARCH}.tar.xz"
  rm -f "arduino-${ARDUINO_VERSION}-${LINUX_ARCH}.tar.xz"

  prefix_print "Installing Arduino IDE..."
  rm -rf arduino-ide/
  mv arduino-${ARDUINO_VERSION}/ arduino-ide/
fi

if [ ! -f arduino-ide/lib/teensyduino.txt ] || [ -n "${full}" ]; then
  apt_install upx-ucl patchelf
  TD_VERSION=td_157

  prefix_print "Downloading Teensyduino (version $TD_VERSION)..."
  rm -f TeensyduinoInstall.${LINUX_ARCH}
  curl -O "https://www.pjrc.com/teensy/$TD_VERSION/TeensyduinoInstall.${LINUX_ARCH}"

  prefix_print "Extracting Teensyduino..."
  upx -d TeensyduinoInstall.${LINUX_ARCH}

  prefix_print "Patching Teensyduino..."
  patchelf --remove-needed libXft.so.2 TeensyduinoInstall.${LINUX_ARCH}
  chmod +x TeensyduinoInstall.${LINUX_ARCH}

  prefix_print "Installing Teensyduino..."
  ./TeensyduinoInstall.${LINUX_ARCH} --dir=arduino-ide
  rm -f TeensyduinoInstall.${LINUX_ARCH}
fi

prefix_print "Patching Teensyduino files..."
for patch_file in patches/*.patch; do
  patch -Ns -p0 -r- -i "${patch_file}" -s | grep -Ev "Skipping|ignored" && return 1
done

if ! command -v wslpath >/dev/null; then
  apt_install teensy-loader-cli
else
  prefix_print "Detected hypervisor: WSL"
  if [ ! -x arduino-ide/hardware/tools/teensy.exe ] || [ -n "${full}" ]; then
    prefix_print "Downloading latest Windows Teensy Loader..."
    rm -f teensy.exe
    curl -O https://www.pjrc.com/teensy/teensy.exe
    chmod +x teensy.exe
    prefix_print "Installing Windows Teensy Loader..."
    mv teensy.exe arduino-ide/hardware/tools/
  fi
fi

if [ -d arduino-ide ] && [ ! -d arduino-ide/libraries/Adafruit_Sensor ] || [ -n "${full}" ]; then
  prefix_print "Installing Adafruit Unified Sensor library..."
  rm -rf arduino-ide/libraries/Adafruit_Sensor/src
  mkdir -p arduino-ide/libraries/Adafruit_Sensor/src
  curl -L https://github.com/adafruit/Adafruit_Sensor/archive/master.tar.gz | tar xz -C arduino-ide/libraries/Adafruit_Sensor/src --strip-components 1
fi

if [ -d arduino-ide ] && [ ! -d arduino-ide/libraries/Adafruit_BusIO ] || [ -n "${full}" ]; then
  prefix_print "Installing Adafruit BusIO library..."
  rm -rf arduino-ide/libraries/Adafruit_BusIO/src
  mkdir -p arduino-ide/libraries/Adafruit_BusIO/src
  curl -L https://github.com/adafruit/Adafruit_BusIO/archive/master.tar.gz | tar xz -C arduino-ide/libraries/Adafruit_BusIO/src --strip-components 1
fi

if [ -d arduino-ide ] && [ ! -d arduino-ide/libraries/Adafruit_BNO055 ] || [ -n "${full}" ]; then
  prefix_print "Installing Adafruit BNO055 library..."
  rm -rf arduino-ide/libraries/Adafruit_BNO055/src
  mkdir -p arduino-ide/libraries/Adafruit_BNO055/src
  curl -L https://github.com/adafruit/Adafruit_BNO055/archive/master.tar.gz | tar xz -C arduino-ide/libraries/Adafruit_BNO055/src --strip-components 1
fi



#--------------------------
# ROS-related dependencies
#

apt_install python3-rosdep python3-wstool

prefix_print "Updating external packages..."
wstool update -t src

if [ ! -f /etc/ros/rosdep/sources.list.d/20-default.list ]; then
  prefix_print "Initializing ROS dependency sources..."
  sudo rosdep init
fi

if [ ! -d ~/.ros/rosdep/sources.cache ] || [ -n "${full}" ]; then
  prefix_print "Updating ROS dependency sources..."
  rosdep update
fi

prefix_print "Installing ROS dependencies..."
rosdep install -qi --from-paths src

if [ "${no_build}" != "1" ]; then
  apt_install build-essential

  prefix_print "Building packages..."
  catkin_make -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -j

  # Environment configuration
  prefix_print "Updating ~/.$(basename "${SHELL}")rc..."
  shellrc=$HOME/.$(basename "${SHELL}")rc
  setupsh=$(realpath "devel/setup.$(basename "${SHELL}")")
  cp "${shellrc}" "${shellrc}.bak"
  grep -v "devel/setup." <"${shellrc}.bak" >"${shellrc}"
  echo "if [ -f \"$setupsh\" ]; then . \"$setupsh\"; fi" >>"${shellrc}"
  rm -f "${shellrc}.bak"

  # echo -e "\n ${BOLD}${BREDB}${BWHT}--->${NC} Please run ${BOLD}${BYEL}. $HOME/.$(basename "${SHELL}")rc${NC} before continuing.${BOLD}${BREDB}${BWHT}<---${NC}\n"
  . "${shellrc}";
fi

prefix_print "All done."
